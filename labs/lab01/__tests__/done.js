const request = require('supertest')
const app = require('../app')

describe("Test multiplication", () => {
  test("It should response the POST method", async () => {
    const payload = { A:[[1,2],[3,4]],B:[[1,2],[3,4]], i:1,j:0 }
    const test = await request(app).post("/")
     .send(payload)
     .set('Content-Type','application/json')
     .expect(200)
    expect(test.body.result).toBe(15)
  })
})
