const express = require('express')
const bodyParser = require('body-parser')
const app = express()

app.use(bodyParser.json())

app.post('/',(req,res) => {
  const { A, B, i, j } = req.body
  var sum = 0
  for(var r=0;r<A[i].length;r++) {
    sum+=A[i][r]*B[r][j]
  // console.log(`${A[i][r]}x${B[r][j]}`)
  }
  return res.json({result:sum})
})

module.exports = app
